# Property Rentals in Brazil

This project is based on a dataset from Kaggle that displays infomation on property rentals in Brazil. To conduct this study, I created a fictional real estate company that wants to launch an online marketplace, enabling customers to select properties to rent with more simplicity and transparency. The firm's owners determined that the company won't work with luxury properties. As there aren't any descriptions in the dataset informing the type of each property (e.g. house, flat, apartment, among others), I established that the firm will only work with properties that have at maximum 4 rooms, 4 bathrooms and 3 parking spaces. This will allow us to practice and apply new strategies to filter our data.

OBS: HOA means 'Homeowners association tax'. Please, take into account that all monetary values are in Brazilian Reais (R$).

To develop this project, I used Python and two visualization libraries (Seaborn and Plotly).


Data Source: https://www.kaggle.com/rubenssjr/brasilian-houses-to-rent?select=houses_to_rent_v2.csv
